#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" 
Python implementation of Gregg&Carder1990 downwelling irradiance atmospheric model, as described in Groetsch2017b, with updated coefficients from Kasten&Young1989, spectrally expanded by Gege2012, and adjacency component by Bird&Riordan1986.

References:
- Groetsch, P. M. M., Gege, P., Simis, S. G. H., Eleveld, M. A., & Peters, S. W. M. (2017). Variability of adjacency effects in sky reflectance measurements. Optics Letters, 42(17), 1–5.
- W. W. Gregg and K. L. Carder, "A simple spectral solar irra- diance model for cloudless maritime atmospheres," Limnol. Oceanogr. 35, 1657–1675 (1990).
- F. Kasten and A. T. Young, "Revised optical air mass tables and approximation formula," Appl. Opt. 28, 4735–4738 (1989).
- Gege, P., "Analytic model for the direct and diffuse components of downwelling spectral irradiance in water," Applied Optics, 51(9), 1407–1419 (2012).
- Bird, R. E., & Riordan, C. (1986). Simple Solar Spectral Model for Direct and Diffuse Irradiance on Horizontal and Tilted Planes at the Earth’s Surface for Cloudless Atmospheres. Journal of Climate and Applied Meteorology, 25, 87–97.
"""

import numpy as np
import pandas as pd
import pylab as pl
import seaborn as sb
import seaborn as sb
sb.set(font='Calibri', font_scale=1.5)
sb.set_style("ticks")
sb.set_palette("deep")

import matplotlib
matplotlib.rcParams['mathtext.fontset'] = 'custom'
matplotlib.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
matplotlib.rcParams['mathtext.it'] = 'Bitstream Vera Sans'
matplotlib.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'

import doctest
import time
import theano as th
import theano.tensor as T
from theano.ifelse import ifelse
import lmfit as lm

"""
__author__ = "Philipp Groetsch"
__copyright__ = "Copyright 2017, Philipp Groetsch"
__license__ = "LGPL"
__version__ = "0.1"
__maintainer__ = "Philipp Groetsch"
__email__ = "philipp.g@gmx.de"
__status__ = "Development"

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


class adj_model(object):

    def __init__(self, window_type='hamming', smoothing=100, std=False, wl = np.arange(350,900,0.1), spectra_path = 'spectra/'):
        """ 
        window_type -- smoothing window type (e.g. boxcar, hamming, gaussian, ...)
        smoothing -- smoothing window length, or False when no smoothing should be applied
        std -- standard deviation, i.e. for Gaussian window
        spectra_path -- path to input spectra 
        wl -- spectral range to initialize the model with
        """

        E0 = pd.read_csv(spectra_path + 'E0_sun.txt', skiprows=10, sep='\t', index_col=0)
        wv = pd.read_csv(spectra_path + 'WV.A', skiprows=3, sep='\t', index_col=0)
        o3 = pd.read_csv(spectra_path + 'O3.A', skiprows=3, sep='\t', index_col=0)
        o2 = pd.read_csv(spectra_path + 'O2.A', skiprows=3, sep='\t', index_col=0)

        r_g_ref = pd.read_csv(spectra_path + 'LwnGrass.dat', sep='\t', index_col=0).iloc[:,0]
        r_g_ref = np.interp(wv.index, r_g_ref.index * 1000, r_g_ref)
        
        d = pd.DataFrame(index = wv.index)
        d.index.name = 'Wavelength, [nm]'
        d['E0_sun'] = E0
        d['WV'] = wv
        d['O2'] = o2
        d['O3'] = o3
        d['r_g_ref'] = r_g_ref
        d['r_g'] = r_g_ref
        
        d = d.apply(lambda x: pd.Series(np.interp(wl, x.index, x), index=wl))

        if smoothing == False:
            self.spectra = d
        else: 
            self.spectra = d.apply(lambda x: x.rolling(window = smoothing, win_type=window_type, center=True, min_periods=1).mean(std=std))
        
        self.spectra = self.spectra.dropna()

        self.f = self.compile_theano_function()


    def compile_theano_function(self):
        """ theano implementation of adjacency sky reflectance model """

        theta_sun, wv, beta, alpha, Hoz, am, rh, p, M_g, doty = T.scalars('theta_sun', 'wv', 'beta', 'alpha', 'Hoz', 'am', 'rh', 'p', 'M_g', 'doty')

        wl, E0_sun, O2, O3, WV, r_g = T.vectors('wl', 'E0_sun', 'O2', 'O3', 'WV', 'r_g')
        
        wl_a = 550

        theta_sun_ = theta_sun * np.pi / 180. 

        z3 = -0.1417 * alpha + 0.82
        z2 = ifelse(T.gt(alpha, 1.2), 0.65, z3)
        z1 = ifelse(T.lt(alpha, 0), 0.82, z2)
        
        theta_sun_mean = z1

        B3 = T.log(1 - theta_sun_mean)
        B2 = B3 * (0.0783 + B3 * (-0.3824 - 0.5874 * B3))
        B1 = B3 * (1.459 + B3 * (0.1595 + 0.4129 * B3))
        Fa = 1 - 0.5 * T.exp((B1 + B2 * T.cos(theta_sun_)) * T.cos(theta_sun_))

        omega_a = (-0.0032 * am + 0.972) * T.exp(3.06 * 1e-4 * rh)
        tau_a = beta*(wl/wl_a)**(-alpha)

        # fixed a bug in M, thanks Jaime! [brackets added]
        M  = 1 / (T.cos(theta_sun_) + 0.50572 * (90 + 6.07995 - theta_sun)**(-1.6364))
        M_ = M * p / 1013.25
        Moz = 1.0035 / (T.cos(theta_sun_)**2 + 0.007)**(0.5)

        Tr = T.exp(- M_ / (115.6406 * (wl / 1000)**4 - 1.335 * (wl / 1000)**2)) 
        Taa = T.exp(- (1 - omega_a) * tau_a * M) 
        Toz = T.exp(- O3 * Hoz * Moz)
        To = T.exp((-1.41 * O2 * M_) / (1 + 118.3 * O2 * M_)**0.45)
        Twv = T.exp((-0.2385 * WV * wv * M) / (1 + 20.07 * WV * wv * M)**0.45)
        Tas = T.exp(- omega_a * tau_a * M)

        F0 = E0_sun * (1 + 0.0167 * T.cos(2 * np.pi * (doty - 3) / 365.))**2

        Edd = F0 * T.cos(theta_sun_) * Tr * Taa * Tas * Toz * To * Twv
        
        Edsr = 0.5 * F0 * T.cos(theta_sun_) * (1 - Tr**0.95) * Taa * Toz * To * Twv

        Edsa = F0 * T.cos(theta_sun_) * Tr**1.5 * Taa * Toz * To * Twv * (1 - Tas) * Fa

        ### r_s with M evaluated at M_g. M_g = M if -1 is parsed. 
        M_g_ = ifelse(T.eq(M_g, -1), M, M_g)
        M_ = M_g_ * p / 1013.25
        Tr = T.exp(- M_ / (115.6406 * (wl / 1000)**4 - 1.335 * (wl / 1000)**2)) 
        Taa = T.exp(- (1 - omega_a) * tau_a * M_g_) 
        To = T.exp((-1.41 * O2 * M_) / (1 + 118.3 * O2 * M_)**0.45)
        Twv = T.exp((-0.2385 * WV * wv * M_g_) / (1 + 20.07 * WV * wv * M_g_)**0.45)
        Tas = T.exp(- omega_a * tau_a * M_g_)

        r_s = To * Twv * Taa * (0.5 * (1 - Tr) + (1 - Fa) * Tr * (1 - Tas)) # Bird1985 eq 19
        
        Edsg = (Edd + Edsr + Edsa) * r_g * r_s / (1 - r_g * r_s) # Bird1985 eq 18

        f = th.function([theta_sun, wv, beta, alpha, Hoz, am, rh, p, M_g, doty, wl, E0_sun, O2, O3, WV, r_g], [Edd, Edsr, Edsa, Edsg, r_s])

        return f



    def fit_LsEd(self, wl, LsEd, theta_sun, am=1, rh=60, pressure=1013.25, wv=2.5, Hoz=0.3, doty=150, M_g=-1, weights=[], params=[], adjacency=True, fit_components=True, fit_method='nelder', fit_options={}, suppress_title=False, plot=False):
        
        """ Fit of the sky reflectance model to a measurement.

        Input:
            wl: wavelength array (numpy array)
            LsEd: sky reflectance (numpy array)
            theta_sun: sun zenith angle
            am: air mass type [1-10]
            rh: relative humidity [%]
            pressure: atmospheric pressure [mbar] 
            wv: water vapour scale height [cm]
            Hoz: Ozone scale height [cm]
            doty: day of year [1-365]
            M_g: fixed relative air mass used for adjacency calculation; parse -1 to use calculated value
            weights: spectral weighting function (numpy array)
            params: lmfit parameter array, [] to use default values
            adjacency: account for adjacency effects in the model run
            fit_components: fit Rayleigh- and Mie-scattering components separately
            fit_method: optimization algorithm (e.g. Nelder-mead)
            fit_options: dictionary of optimization flags
            suppress_title: do not add title to plots (True/False)
            plot: create a plot of model fit results (True/False)

        Output:
            reg: lmfit minimizer result set
            LsEd_modeled: modelled sky reflectance (numpy array)
            Edd_Ed: modelled direct reflectance (numpy array)
            Edsr_Ed: modelled diffuse Rayleigh-scattered reflectance (numpy array)
            Edsa_Ed: modelled diffuse aerosol-scattered reflectance (numpy array)
            Edsg_Ed: modelled diffuse adjacency reflectance (numpy array)
            r_s: modelled sky reflectivity (numpy array)
        """


        if params == []:
            params = lm.Parameters()
            # (Name,  Value,  Vary,   Min,  Max,  Expr)
            params.add_many(
                ('theta_sun', theta_sun, False, 0, 90, None),         
                ('alpha', 1, True, 0, 3, None),
                ('beta', 0.1, True, 0.001, 1, None),
                ('am', am, False, 1, 10, None),
                ('rh', rh, False, 0, 100, None),
                ('pressure', pressure, False, 800, 1100, None),
                ('wv', wv, False, 0, 10, None),
                ('Hoz', Hoz, False, 0, 10, None),
                ('doty', doty, False, 1, 365, None),
                ('fdd', 0, False, 0, 10, None),
                ('fdsr', 1, True, 0, 10, None),
                ('fdsa', 1, fit_components, 0, 10, None if fit_components else 'fdsr'),
                ('fdsg', 1 if adjacency else 0, adjacency, 0, 10, None),
                ('M_g', M_g, False, -1, 10, None),
                )


        def min_funct(params):
            p = params.valuesdict()

            Edd, Edsr, Edsa, Edsg, r_s = self.f(p['theta_sun'], p['wv'], p['beta'], p['alpha'], p['Hoz'], p['am'], p['rh'], p['pressure'], p['M_g'], p['doty'], wl, self.spectra.E0_sun, self.spectra.O2, self.spectra.O3, self.spectra.WV, self.spectra.r_g)

            if not adjacency:
                Edsg *= 0

            Ed = Edd + Edsr + Edsa + Edsg
            Edd_Ed = Edd / Ed * p['fdd']
            Edsr_Ed = Edsr / Ed * p['fdsr']
            Edsa_Ed = Edsa / Ed * p['fdsa']
            Edsg_Ed = Edsg / Ed * p['fdsg']

            LsEd_modeled = (Edsa_Ed + Edsr_Ed + Edsg_Ed) * 1/np.pi
            
            # Least squares
            RSS = np.sum((LsEd - LsEd_modeled)**2 * weights)
            
            return RSS, LsEd_modeled, Edd_Ed, Edsr_Ed, Edsa_Ed, Edsg_Ed, r_s
        
        if weights == []:
            weights = np.ones(wl.size)
        
        reg = lm.minimize(lambda x: min_funct(x)[0], params=params, method=fit_method, options=fit_options)  

        RSS, LsEd_modeled, Edd_Ed, Edsr_Ed, Edsa_Ed, Edsg_Ed, r_s = min_funct(reg.params)

        RMSD = np.sqrt(((LsEd_modeled - LsEd)**2).sum() / LsEd.size)
        NRMSD = np.sqrt(((LsEd_modeled - LsEd)**2).sum() / LsEd.size) / LsEd.mean() * 100

        if plot:
            pl.figure(figsize=(8,4))
            pl.plot(wl, LsEd, 'k--', linewidth=3, alpha=0.8, label='$L_{sky} / E_d$')
            pl.plot(wl, LsEd_modeled, 'b', label='Fit')
            pl.plot(wl, 1/np.pi * (Edsa_Ed + Edsr_Ed), 'g', label='$(L_{dsr} + L_{dsa}) / E_d$')
            #pl.plot(wl, 1/np.pi * Edsa_Ed, label='$E_{dsa} / E_d$')
            #pl.plot(wl, 1/np.pi * Edsr_Ed, label='$E_{dsr} / E_d$')
            if adjacency:
                pl.plot(wl, 1/np.pi * Edsg_Ed * 10, 'r', label='$L_{dsg} / E_d \\times$ 10')
                # pl.plot(wl, self.spectra.r_g * LsEd.max(), label='$r_{g} \\times$ %1.2f' % LsEd.max())
            #pl.plot(wl, r_s, label='r_s')
            pl.plot(wl, 10 * (LsEd - LsEd_modeled), 'k', label='$R_{resid} \\times$ 10', alpha=0.5)
            pl.hlines(0, wl[0], wl[-1], linewidth=1, linestyles='dashed', alpha=0.5)
            pl.legend(ncol=2)
            if not suppress_title:
                if adjacency:
                    pl.title('$\\alpha:$ %1.2g, $\\beta:$ %1.2g, $f_{dsr}$: %1.2g, $f_{dsa}$: %1.2g, $f_{dsg}:$ %1.2g\nRSS: %1.2g $sr^{-1}$, RMSD: %1.2g $sr^{-1}$, NRMSD: %1.2g %%' % (reg.params['alpha'], reg.params['beta'], reg.params['fdsr'], reg.params['fdsa'], reg.params['fdsg'], RSS, RMSD, NRMSD))
                else:
                    pl.title('$\\alpha:$ %1.2g, $\\beta:$ %1.2g, $f_{dsr}$: %1.2g, $f_{dsa}$: %1.2g, $f_{dsg}:$ %1.2g\nRSS: %1.2g $sr^{-1}$, RMSD: %1.2g $sr^{-1}$, NRMSD: %1.2g %%' % (reg.params['alpha'], reg.params['beta'], reg.params['fdsr'], reg.params['fdsa'], reg.params['fdsg'], RSS, RMSD, NRMSD))
            pl.xlim(wl[0], wl[-1])
            pl.ylabel('Reflectance, [$sr^{-1}$]')
            pl.xlabel('Wavelength, [nm]')
            sb.despine()
            pl.tight_layout()            
            pl.show()
            
            print('---------------------')            
            reg.params.pretty_print()
            print('---------------------')

        return reg, LsEd_modeled, Edd_Ed, Edsr_Ed, Edsa_Ed, Edsg_Ed, r_s

    def fit_Ed(self, wl, Ed, theta_sun, fit_Ls=False, am=1, rh=60, pressure=1013.25, wv=2.5, Hoz=0.3, doty=150, M_g=-1, weights=[], params=[], adjacency=True, fit_components=False, fit_method='nelder', fit_options={}, plot=False):
        """ Fit of the model to an irradiance or radiance measurement. 

        Not fully tested, use with caution! 

        Input:
            wl: wavelength array (numpy array)
            Ed: sky reflectance (numpy array)
            theta_sun: sun zenith angle
            fit_Ls: True if sky radiance is provided, False if downwelling irradiance is provided
            am: air mass type [1-10]
            rh: relative humidity [%]
            pressure: atmospheric pressure [mbar] 
            wv: water vapour scale height [cm]
            Hoz: Ozone scale height [cm]
            doty: day of year [1-365]
            M_g: fixed relative air mass used for adjacency calculation; parse -1 to use calculated value
            weights: spectral weighting function (numpy array)
            params: lmfit parameter array, [] to use default values
            adjacency: account for adjacency effects in the model run
            fit_components: fit Rayleigh- and Mie-scattering components separately
            fit_method: optimization algorithm (e.g. Nelder-mead)
            fit_options: dictionary of optimization flags
            suppress_title: do not add title to plots (True/False)
            plot: create a plot of model fit results (True/False)

        Output:
            reg: lmfit minimizer result set
            Ed_modeled: modelled downwelling irradiance (numpy array)
            Edd: modelled direct irradiance (numpy array)
            Edsr: modelled diffuse Rayleigh-scattered irradiance (numpy array)
            Edsa: modelled diffuse aerosol-scattered irradiance (numpy array)
            Edsg: modelled diffuse adjacency irradiance (numpy array)
            r_s: modelled sky reflectivity (numpy array)
        """

        if params == []:
            params = lm.Parameters()
            # (Name,  Value,  Vary,   Min,  Max,  Expr)
            params.add_many(
                ('theta_sun', theta_sun, False, 0, 90, None),         
                ('alpha', 1, True, 0, 3, None),
                ('beta', 0.1, True, 0.001, 1, None),
                ('am', am, False, 1, 10, None),
                ('rh', rh, False, 0, 100, None),
                ('pressure', pressure, False, 800, 1100, None),
                ('wv', wv, True, 0, 10, None),
                ('Hoz', Hoz, True, 0, 10, None),
                ('doty', doty, False, 1, 365, None),
                ('fdd', 0 if fit_Ls else 1, False if fit_Ls else True, 0, 10, None),
                ('fdsr', 1, True, 0, 10, None),
                ('fdsa', 1, fit_components, 0, 10, None if fit_components else 'fdsr'),
                ('fdsg', 1 if adjacency else 0, adjacency, 0, 10, None),
                ('M_g', M_g, False, -1, 10, None),
                )


        def min_funct(params):
            p = params.valuesdict()

            Edd, Edsr, Edsa, Edsg, r_s = self.f(p['theta_sun'], p['wv'], p['beta'], p['alpha'], p['Hoz'], p['am'], p['rh'], p['pressure'], p['M_g'], p['doty'], wl, self.spectra.E0_sun, self.spectra.O2, self.spectra.O3, self.spectra.WV, self.spectra.r_g)

            Edd *= p['fdd']
            Edsr *= p['fdsr']
            Edsa *= p['fdsa']
            Edsg *= p['fdsg']

            Ed_modeled = Edd + Edsr + Edsa + Edsg            
            
            # Least squares
            RSS = np.sum((Ed - Ed_modeled)**2 * weights)
            
            return RSS, Ed_modeled, Edd, Edsr, Edsa, Edsg, r_s
        
        if weights == []:
            weights = np.ones(wl.size)
        
        reg = lm.minimize(lambda x: min_funct(x)[0], params=params, method=fit_method, options=fit_options)

        RSS, Ed_modeled, Edd, Edsr, Edsa, Edsg, r_s = min_funct(reg.params)

        RMSD = np.sqrt(((Ed_modeled - Ed)**2).sum() / Ed.size)
        NRMSD = np.sqrt(((Ed_modeled - Ed)**2).sum() / Ed.size) / Ed.mean() * 100

        if plot:
            pl.figure(figsize=(8,4))
            pl.plot(wl, Ed_modeled, label='Fit')
            pl.plot(wl, Ed, label='$L_{sky}$' if fit_Ls else '$E_d$')
            if not fit_Ls:
                pl.plot(wl, Edd, label='$E_{dd}$')
            pl.plot(wl, Edsa + Edsr, label='$E_{ds}$')
            #pl.plot(wl, Edsr, label='Edsr')
            if adjacency:
                pl.plot(wl, Edsg * 10, label='$E_{dsg} \\times$ 10')
                #pl.plot(wl, self.spectra.r_g * Ed.max(), label='$r_{g} \\times$ %1.2f' % Ed.max())
            pl.plot(wl, (Ed - Ed_modeled) * 10, 'k', label='$E_{resid} \\times$ 10', alpha=0.5)
            pl.hlines(0, wl[0], wl[-1], linewidth=1, linestyles='dashed', alpha=0.5)
            pl.legend(ncol=2)            
            if not suppress_title:
                pl.title('$\\alpha:$ %1.2g, $\\beta:$ %1.2g, $f_{dsr}$: %1.2g, $f_{dsa}$: %1.2g, $f_{dsg}:$ %1.2g, $H_{oz}:$ %1.2g, $WV:$ %1.2g\nRSS: %1.2g $sr^{-1}$, RMSD: %1.2g $sr^{-1}$, NRMSD: %1.2g %%' % (reg.params['alpha'], reg.params['beta'], reg.params['fdsr'], reg.params['fdsa'], reg.params['fdsg'], reg.params['Hoz'], reg.params['wv'], RSS, RMSD, NRMSD))
            pl.xlim(wl[0], wl[-1])
            if fit_Ls:
                pl.ylabel('Radiance, [$mW nm^{-1} m^{-2} sr^{-1}$]')
            else:
                pl.ylabel('Irradiance, [mW nm^{-1} m^{-2}]')
            pl.xlabel('Wavelength, [nm]')
            sb.despine()
            pl.tight_layout()            
            pl.show()

            print('---------------------')
            reg.params.pretty_print()
            print('---------------------')

        return reg, Ed_modeled, Edd, Edsr, Edsa, Edsg, r_s



if __name__ == '__main__':    
    
    data = pd.read_csv('example_data.csv', index_col=0, skiprows=9)
    wl = data.index.values
    Ls = data.iloc[:,0].values
    Ed = data.iloc[:,1].values
    theta_sun = 40.62

    gc = adj_model(wl=wl)

    reg, LsEd_modeled, Edd_Ed, Edsr_Ed, Edsa_Ed, Edsg_Ed, r_s = gc.fit_LsEd(gc.spectra.index, Ls/Ed, theta_sun, plot=True)
